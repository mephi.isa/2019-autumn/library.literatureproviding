from typing import Union, NoReturn
from collections import namedtuple

from src.utils import *

TYPE_PAPER = 0
TYPE_ELECTRONIC = 1


class Name:
    def __init__(self,
                 name: str,
                 surname: str,
                 middle: str):
        if not (
                isinstance(name, str) and isinstance(surname, str)
                and isinstance(middle, str)
        ):
            raise TypeError
        self.name = name
        self.surname = surname
        self.middle = middle

    def __eq__(self, other):
        if not isinstance(other, Name):
            raise TypeError
        else:
            return (
                    self.name == other.name and
                    self.middle == other.middle and
                    self.surname == other.surname
            )

    def __ne__(self, other):
        if not isinstance(other, Name):
            raise TypeError
        else:
            return not self.__eq__(other)


class CopyInOrder:
    def __init__(self, type_, index, link, due_date, returned=False):
        # type is one of TYPE_PAPER or TYPE_ELECTRONIC
        if not (
                isinstance(type_, int) and isinstance(index, int)
                and is_datetime_valid(due_date) and isinstance(returned, bool) and (
                        (type_ == TYPE_PAPER and link is None) or
                        (type_ == TYPE_ELECTRONIC and isinstance(link, str))
                )
        ):
            raise TypeError
        self.type_ = type_
        self.index = index
        self.link = link
        self.due_date = due_date
        self.returned = returned

    def __eq__(self, other):
        if not isinstance(other, CopyInOrder):
            raise TypeError
        return (self.type_ == other.type_ and
                self.index == other.index and
                self.link == other.link and
                self.due_date == other.due_date and
                self.returned == other.returned)

    def __ne__(self, other):
        if not isinstance(other, CopyInOrder):
            raise TypeError
        else:
            return not self.__eq__(other)


class Copy:
    pass


class Order:
    def __init__(self, date: str):
        if not isinstance(date, str):
            raise TypeError
        if not is_datetime_valid(date):
            raise ValueError
        self.date = date
        # copies is a list of CopyInOrder objects
        self.copies = []

    def __eq__(self, other):
        if not isinstance(other, Order):
            raise TypeError
        if self.date != other.date or len(self.copies) != len(other.copies):
            return False
        for copy1, copy2 in zip(self.copies, other.copies):
            if copy1 != copy2:
                return False
        return True

    def __ne__(self, other):
        if not isinstance(other, Order):
            raise TypeError
        else:
            return not self.__eq__(other)

    def add_copy(self, type_: int, index: int, link: str, due_date: str) -> bool:
        try:
            new_copy_in_order = CopyInOrder(type_, index, link, due_date)
        except AssertionError:
            return False
        index_info = self.get_copy_by_index(new_copy_in_order.index)
        if index_info is not None:
            return False
        self.copies.append(new_copy_in_order)
        return True

    def get_copy_by_index(self, index: int) -> Union[CopyInOrder, None, NoReturn]:
        if not isinstance(index, int):
            raise TypeError
        for copy in self.copies:
            if copy.index == index:
                return copy
        return None

    def get_copy_index_by_link(self, link: str) -> Union[int, None]:
        """
        :rtype: index (if index found) or None (if index not found or link is not string)
        """
        if not isinstance(link, str): return None
        copies = [copy_in_order
                  for copy_in_order in self.copies
                  if copy_in_order.type_ == TYPE_ELECTRONIC
                  and copy_in_order.link == link
                  and is_utc_datetime_in_future(copy_in_order.due_date)]
        assert len(copies) <= 1
        if len(copies) == 0:
            return None
        return copies[0].index

    def set_copy_returned(self, copy_index: int, new_val: bool):
        copies = [copy_in_order
                  for copy_in_order in self.copies
                  if copy_in_order.index == copy_index]
        assert len(copies) <= 1
        if len(copies) == 0:
            return False
        copies[0].returned = new_val
        return True


class Reader:
    DEFAULT_CARD_DURATION_DAYS = 90

    def __init__(self,
                 name: Name,
                 card_num: int,
                 passport_info: str,
                 registration_datetime=None,
                 card_expiration_date=None):
        self.card_num = card_num

        if registration_datetime is None:
            self.registration_datetime = get_cur_utc_datetime()
        elif not is_datetime_valid(registration_datetime):
            raise ValueError
        else:
            self.registration_datetime = registration_datetime

        if card_expiration_date is None:
            self.card_expiration_date = \
                get_due_utc_datetime(get_cur_utc_datetime(), Reader.DEFAULT_CARD_DURATION_DAYS)
        elif not is_datetime_valid(card_expiration_date):
            raise ValueError
        else:
            self.card_expiration_date = card_expiration_date

        self.passport_info = passport_info
        self.name = name

        # list of Order objects
        self._orders = []
        self._is_violator = False

        # number of taken paper copies
        self._taken_paper_copies = 0

    def add_order(self, order: Order):
        if not isinstance(order, Order):
            raise TypeError
        for copy in order.copies:
            assert not copy.returned
            if copy.type_ == TYPE_PAPER:
                self._taken_paper_copies += 1
        self._orders.append(order)

    def get_copy_index_by_link(self, link: str):
        if not isinstance(link, str):
            raise TypeError
        for order in self._orders:
            copy_index = order.get_copy_index_by_link(link)
            if copy_index is not None:
                return copy_index
        return None

    def set_copy_returned(self, copy_index: int, new_val: bool) -> bool:
        if not isinstance(copy_index, int) or not isinstance(new_val, bool):
            raise TypeError
        for order in self._orders:
            if order.set_copy_returned(copy_index, new_val):
                return True
        return False

    # used for WHAT???
    # def has_non_returned_copy(self, index: int) -> bool:
    #     if not isinstance(index, int):
    #         raise TypeError
    #     for order in self._orders:
    #         index_info = order.get_index_in_order_info(index)
    #         if index_info.exists and not index_info.returned:
    #             return True
    #     return False


class Employee:
    def __init__(self,
                 work_function_: str,
                 password: str):
        if not isinstance(work_function_, str) or not isinstance(password, str):
            raise TypeError
        self.work_function = work_function_
        self._password = password

    def __eq__(self, other):
        if not isinstance(other, Employee):
            raise TypeError
        return (
                self.work_function == other.work_function and
                self._password == other._password
        )

    def __ne__(self, other):
        if not isinstance(other, Employee):
            raise TypeError
        return not self.__eq__(other)


class Copy:
    def __init__(self,
                 index: int,
                 author: str,
                 title: str):
        if not isinstance(index, int) \
                or not isinstance(author, str) \
                or not isinstance(title, str):
            raise TypeError
        self.index = index
        self.author = author
        self.title = title

    def type(self):
        if type(self) == ElectronicCopy:
            return TYPE_ELECTRONIC
        elif type(self) == PaperCopy:
            return TYPE_PAPER
        else:
            raise TypeError

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError
        return (
                self.index == other.index and
                self.author == other.author and
                self.title == other.title
        )

    def __ne__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError
        return not self.__eq__(other)


class ElectronicCopy(Copy):
    def __init__(self, index: int, author: str, title: str):
        """
        :param index:
        :param author:
        :param title:
        """
        super().__init__(index, author, title)
        self._opened_sessions = 0

    def get_link(self):
        # TODO: make real link generator. something like "electronic/<index>"
        return "http://fake_link/{}".format(self.index)
    #
    # def open_session(self) -> bool:
    #     self._opened_sessions += 1
    #     return True
    #
    # def close_session(self) -> bool:
    #     if self._opened_sessions > 0:
    #         self._opened_sessions -= 1
    #         return True
    #     else:
    #         return False


class PaperCopy(Copy):
    def __init__(self, index: int, author: str, title: str, ):
        super().__init__(index, author, title)
        # next fields are initialized by default vals
        self._is_given = False

    def set_given(self, new_val: bool):
        if not isinstance(new_val, bool):
            raise TypeError
        self._is_given = new_val


class LiteratureProvider:
    def __init__(self):
        # key - card_number, value - objects
        self._readers = dict()
        self._copies = dict()

    # load readers from database
    def _load_readers(self):
        raise NotImplemented

    # load copies from database
    def _load_copies(self):
        raise NotImplemented

    def _get_copy(self, index: int) -> Copy:
        if not isinstance(index, int):
            raise TypeError
        return self._copies.get(index, None)

    def get_reader(self, card_num: int) -> Reader:
        if not isinstance(card_num, int):
            raise TypeError
        return self._readers.get(card_num, None)

    def _add_reader(self, reader: Reader):
        if not isinstance(reader, Reader):
            raise TypeError
        self._readers[reader.card_num] = reader

    # TODO: make more complex readers comparsion?
    def _is_same_reader_exists(self, reader : Reader) -> bool:
        if not isinstance(reader, Reader):
            raise TypeError
        for existing_reader in self._readers.values():
            if existing_reader.passport_info == reader.passport_info:
                return True
        return False

    def register_reader(self, reader: Reader) -> bool:
        if not isinstance(reader, Reader):
            raise TypeError
        if self._is_same_reader_exists(reader):
            return False
        else:
            self._add_reader(reader)
            return True

    def login_employee(self, work_function, password):
        raise NotImplemented

    # how is due_date for each copy is passed?
    def create_order(self, reader_card: int, copy_indices: list) -> bool:
        reader = self.get_reader(reader_card)
        if reader is None:
            raise NotImplemented

        order = Order(get_cur_utc_datetime())
        for copy_index in copy_indices:
            copy = self._get_copy(copy_index)
            if copy is None:
                raise NotImplemented
            if copy.type() == TYPE_ELECTRONIC:
                link = copy.get_link()
            elif copy.type() == TYPE_PAPER:
                copy.set_given(True)
                link = None
            else:
                raise TypeError

            order.add_copy(copy.type(), copy.index, link, get_due_utc_datetime(get_cur_utc_datetime()))

        reader.add_order(order)
        return True

    def receive_copy(self, reader_card: int, copy_index: int) -> bool:
        reader = self.get_reader(reader_card)
        if reader is None:
            raise NotImplemented
        copy = self._get_copy(copy_index)
        if copy is None:
            raise NotImplemented
        return reader.set_copy_returned(copy_index, True)

    def get_violators(self) -> list:
        raise NotImplemented

    def get_copy_by_link(self, user_card_num: int, link: str):
        reader = self.get_reader(user_card_num)
        if reader is None:
            return None
        copy_index = reader.get_copy_index_by_link(link)
        return self._get_copy(copy_index)
