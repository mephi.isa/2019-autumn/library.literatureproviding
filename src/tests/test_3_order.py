import pytest

from src.classes import Order, CopyInOrder, TYPE_PAPER, TYPE_ELECTRONIC
from src.utils import *


# @pytest.

def test_0_0_init_correct():
    date = get_cur_utc_datetime()
    order = Order(date)
    assert order.date == date \
           and isinstance(order.copies, list) and len(order.copies) == 0


def test_0_1_init_ve():
    date = get_cur_utc_datetime() + "lalala"
    with pytest.raises(ValueError):
        order = Order(date)


def test_0_2_init_wrong_date_type():
    date = 42
    with pytest.raises(TypeError):
        order = Order(date)


def test_1_0_add_copy_correct():
    order = Order(get_cur_utc_datetime())
    type_ = TYPE_PAPER
    index = 1
    link = None
    due_date = get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=10)
    res = order.add_copy(type_, index, link, due_date)
    assert (
            res and
            order.copies == [CopyInOrder(type_, index, link, due_date)]
    )


def test_1_2_add_copy_index_dup():
    order = Order(get_cur_utc_datetime())
    type_ = TYPE_PAPER
    index = 1
    link = None
    due_date = get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=10)
    # adding first book
    res = order.add_copy(type_, index, link, due_date)
    assert res
    # adding second book with same index
    res = order.add_copy(TYPE_ELECTRONIC, index, "some_link", due_date)
    assert (
            not res and
            order.copies == [CopyInOrder(type_, index, link, due_date)]
    )


@pytest.fixture
def order_with_two_books():
    order = Order(get_cur_utc_datetime())
    types_ = [TYPE_ELECTRONIC, TYPE_PAPER]
    indices = [0, 1]
    links = ["http://fake_link/0", None]
    due_dates = [
        get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=10),
        get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=15)
    ]
    for i in range(2):
        res = order.add_copy(types_[i], indices[i], links[i], due_dates[i])
        assert res

    return order


def test_2_0_get_copy_index_by_link_correct(order_with_two_books):
    order = order_with_two_books
    tgt_index = order.get_copy_index_by_link("http://fake_link/0")
    assert tgt_index == 0


def test_2_1_get_copy_index_by_link_link_is_none(order_with_two_books):
    order = order_with_two_books
    tgt_index = order.get_copy_index_by_link(None)
    assert tgt_index is None


def test_2_2_get_copy_index_by_link_invalid_link(order_with_two_books):
    order = order_with_two_books
    tgt_index = order.get_copy_index_by_link("non_existing_link")
    assert tgt_index is None


def test_3_0_set_copy_returned_correct(order_with_two_books):
    order = order_with_two_books
    res = order.set_copy_returned(0, True)
    assert res and order.copies[0].returned


def test_3_1_set_copy_returned_invalid_index(order_with_two_books):
    order = order_with_two_books
    res = order.set_copy_returned(3, True)
    assert not res


def test_4_0_eq_correct_date_diff(order_with_two_books):
    order1 = order_with_two_books
    order2 = order_with_two_books

    order3 = Order(get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=4))
    types_ = [TYPE_ELECTRONIC, TYPE_PAPER]
    indices = [0, 1]
    links = ["http://fake_link/0", None]
    due_dates = [
        get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=10),
        get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=15)
    ]
    for i in range(2):
        order3.add_copy(types_[i], indices[i], links[i], due_dates[i])

    assert order1 == order2
    assert not order1 == order3


def test_4_1_eq_correct_copies_len_diff(order_with_two_books):
    order1 = order_with_two_books

    order2 = Order(get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=4))
    types_ = [TYPE_ELECTRONIC, TYPE_PAPER]
    indices = [0, 1]
    links = ["http://fake_link/0", None]
    due_dates = [
        get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=10),
        get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=15)
    ]
    for i in range(2):
        order2.add_copy(types_[i], indices[i], links[i], due_dates[i])
    order2.add_copy(TYPE_PAPER, 2,
                    None, get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=10))

    assert not order1 == order2


def test_4_2_eq_correct_copies_diff(order_with_two_books):
    order1 = order_with_two_books
    order2 = Order(get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=4))
    types_ = [TYPE_ELECTRONIC, TYPE_PAPER]
    indices = [0, 3]
    links = ["http://fake_link/0", None]
    due_dates = [
        get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=10),
        get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=15)
    ]
    for i in range(2):
        order2.add_copy(types_[i], indices[i], links[i], due_dates[i])

    assert not order1 == order2


def test_5_0_get_copy_by_index_correct(order_with_two_books):
    assert order_with_two_books.get_copy_by_index(0) == order_with_two_books.copies[0]
    assert order_with_two_books.get_copy_by_index(1) == order_with_two_books.copies[1]
    assert order_with_two_books.get_copy_by_index(555) is None


def test_5_1_get_copy_by_index_type_error(order_with_two_books):
    with pytest.raises(TypeError):
        order_with_two_books.get_copy_by_index("not-int")
