import pytest

from src.classes import PaperCopy


def test_0_0_init_correct():
    index = 1
    author = "auth"
    title = "title"
    pc1 = PaperCopy(index, author, title)
    assert (
            isinstance(pc1, PaperCopy) and
            pc1.index == index and
            pc1.author == author and
            pc1.title == title and
            not pc1._is_given
    )


def test_0_1_init_type_error():
    index = 1
    author = "auth"
    title = "title"
    with pytest.raises(TypeError):
        PaperCopy("not-int", author, title)
    with pytest.raises(TypeError):
        PaperCopy(index, 123, title)
    with pytest.raises(TypeError):
        PaperCopy(index, author, 321)


@pytest.fixture
def basic_paper_copy():
    index = 1
    author = "auth"
    title = "title"
    return PaperCopy(index, author, title)


def test_1_0_set_given(basic_paper_copy):
    basic_paper_copy.set_given(True)
    assert basic_paper_copy._is_given


