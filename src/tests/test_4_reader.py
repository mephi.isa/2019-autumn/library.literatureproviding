import pytest

from src.classes import Name, Reader
from src.tests.test_3_order import order_with_two_books


def test_0_0_init_corect():
    name = Name("name", "surname", "middle_name")
    card_num = 123
    passport_info = "passport"
    reader = Reader(name, card_num, passport_info)
    assert (
            isinstance(reader, Reader) and
            reader.card_num == card_num and
            reader.passport_info == passport_info and
            reader.name == name and
            isinstance(reader._orders, list) and len(reader._orders) == 0 and
            not reader._is_violator and
            reader._taken_paper_copies == 0
    )


def test_0_1_init_incorrect():
    name = Name("name", "surname", "middle_name")
    card_num = 123
    passport_info = "passport"
    datetime = "not-valid-date"
    with pytest.raises(ValueError):
        Reader(name, card_num, passport_info, datetime)


@pytest.fixture
def usual_reader():
    name = Name("name", "surname", "middle_name")
    card_num = 123
    passport_info = "passport"
    date = "2019-03-22T12:22:00"
    return Reader(name, card_num, passport_info, date)


def test_1_0_add_order(usual_reader, order_with_two_books):
    # setting copy[1].returned because of pytest fixture specific
    order_with_two_books.copies[1].returned = False
    usual_reader.add_order(order_with_two_books)
    assert (
            len(usual_reader._orders) == 1 and
            usual_reader._orders[0] == order_with_two_books and
            usual_reader._taken_paper_copies == 1
    )


def test_1_1_add_order_type_error(usual_reader):
    with pytest.raises(TypeError):
        usual_reader.add_order("not-an-order")
    assert (
            len(usual_reader._orders) == 0 and
            usual_reader._taken_paper_copies == 0
    )


def test_1_2_add_order_incorrect_returned_val_in_copy(usual_reader, order_with_two_books):
    order_with_two_books.copies[1].returned = True
    with pytest.raises(AssertionError):
        usual_reader.add_order(order_with_two_books)
    assert (
            len(usual_reader._orders) == 0 and
            usual_reader._taken_paper_copies == 0
    )


def test_2_0_get_copy_index_by_link_correct(usual_reader, order_with_two_books):
    assert usual_reader.get_copy_index_by_link("any_link") is None
    usual_reader.add_order(order_with_two_books)
    assert usual_reader.get_copy_index_by_link("default_link") == 0
    assert usual_reader.get_copy_index_by_link("non-existent-link") is None


def test_2_1_get_copy_index_by_link_type_error(usual_reader):
    with pytest.raises(TypeError):
        usual_reader.get_copy_index_by_link(1234567)


def test_3_0_set_copy_returned_correct(usual_reader, order_with_two_books):
    usual_reader.add_order(order_with_two_books)
    # checking initial state
    assert not usual_reader._orders[0].copies[0].returned
    # set_to_true
    res = usual_reader.set_copy_returned(0, True)
    assert res and usual_reader._orders[0].copies[0].returned
    # set to false
    res = usual_reader.set_copy_returned(0, False)
    assert res and not usual_reader._orders[0].copies[0].returned
    # wrong index
    res = usual_reader.set_copy_returned(33333, True)
    assert not res


def test_3_1_set_copy_returned_type_error(usual_reader):
    with pytest.raises(TypeError):
        usual_reader.set_copy_returned("not-an-int", True)
    with pytest.raises(TypeError):
        usual_reader.set_copy_returned(1, "not-a-bool")

#
# def test_4_0_has_non_returned_copy_correct(usual_reader, order_with_two_books):
#     # with empty reader
#     assert not usual_reader.has_non_returned_copy(1)
#     # with filled reader
#     usual_reader.add_order(order_with_two_books)
#     usual_reader.set_copy_returned()
#     assert usual_reader.has_non_returned_copy(0)
#     assert not usual_reader.has_non_returned_copy(1)
#
#
# def test_4_1_has_non_returned_copy_type_error(usual_reader):
#     with pytest.raises(TypeError):
#         usual_reader.has_non_returned_copy("not-an-int")
