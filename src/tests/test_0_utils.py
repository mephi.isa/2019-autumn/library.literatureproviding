import pytest

from src.utils import *


def test_0_0_is_datetime_valid_true():
    assert is_datetime_valid("2033-12-03T22:03:32")


def test_0_1_is_datetime_valid_false():
    assert not is_datetime_valid("2020-33-00Trr:ew:ww")
    assert not is_datetime_valid(1111)


def test_1_0_is_utc_datetime_in_future_correct():
    assert is_utc_datetime_in_future("2033-12-03T22:03:32")
    assert not is_utc_datetime_in_future("2000-12-03T22:03:32")


def test_1_1_is_utc_datetime_in_future_incorrect():
    with pytest.raises(AssertionError):
        is_utc_datetime_in_future(123)
    with pytest.raises(ValueError):
        is_utc_datetime_in_future("invalid_string")


def test_2_0_get_cur_utc_datetime_correct():
    assert is_datetime_valid(get_cur_utc_datetime())


def test_3_0_get_due_utc_datetime_correct():
    start_datetime = "2033-12-03T22:03:32"
    days_to_add = 5
    res_date = get_due_utc_datetime(start_datetime)
    assert res_date == "2033-12-17T22:03:32"
    res_date = get_due_utc_datetime(start_datetime, days_to_add)
    assert res_date == "2033-12-08T22:03:32"


def test_3_1_get_due_utc_datetime_type_error():
    start_datetime = "not-a-utc-date"
    with pytest.raises(ValueError):
        get_due_utc_datetime(start_datetime)
    start_datetime = "2033-12-03T22:03:32"
    days_to_add = "not-an-int"
    with pytest.raises(AssertionError):
        get_due_utc_datetime(start_datetime, days_to_add)
