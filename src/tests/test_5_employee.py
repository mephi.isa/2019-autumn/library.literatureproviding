import pytest

from src.classes import Employee


def test_0_0_init_correct():
    work_func = "work_func"
    passwd = "passwd"
    e1 = Employee(work_func, passwd)
    assert (
            isinstance(e1, Employee) and
            e1.work_function == work_func and
            e1._password == passwd
    )


def test_0_1_init_type_error():
    with pytest.raises(TypeError):
        Employee(123, "passwd")
    with pytest.raises(TypeError):
        Employee("work_func", 321)


def test_1_0_eq_correct():
    work_func = "work_func"
    passwd = "passwd"
    e1 = Employee(work_func, passwd)
    e2 = Employee(work_func, passwd)
    e3 = Employee(work_func, "another passwd")
    assert e1 == e2
    assert not e1 == e3


def test_1_1_eq_type_error():
    work_func = "work_func"
    passwd = "passwd"
    e1 = Employee(work_func, passwd)
    with pytest.raises(TypeError):
        e1 == 123

