import pytest

from src.classes import LiteratureProvider, ElectronicCopy, PaperCopy, Reader, Name, TYPE_ELECTRONIC, TYPE_PAPER
from src.tests.test_3_order import order_with_two_books
from src.tests.test_4_reader import usual_reader


def test_0_0_init_correct():
    lp = LiteratureProvider()
    assert (
            isinstance(lp, LiteratureProvider) and
            isinstance(lp._readers, dict) and len(lp._readers) == 0 and
            isinstance(lp._copies, dict) and len(lp._copies) == 0
    )


@pytest.fixture
def basic_literature_provider(usual_reader, order_with_two_books):
    lp = LiteratureProvider()

    reader = usual_reader
    reader.add_order(order_with_two_books)
    lp._readers[reader.card_num] = reader

    copy1 = PaperCopy(1, "paper_author", "some title")
    copy2 = ElectronicCopy(0, "electronic author", "anoher title")
    lp._copies[copy1.index] = copy1
    lp._copies[copy2.index] = copy2
    return lp


def test_1_0_get_copy_correct(basic_literature_provider):
    copy = basic_literature_provider._get_copy(0)
    assert copy == ElectronicCopy(0, "electronic author", "anoher title")
    assert basic_literature_provider._get_copy(2) is None


def test_1_1_get_copy_type_error(basic_literature_provider):
    with pytest.raises(TypeError):
        basic_literature_provider._get_copy('not-an-int')


def test_2_0_get_reader_correct(basic_literature_provider, usual_reader):
    reader = basic_literature_provider.get_reader(123)
    assert reader == usual_reader
    assert basic_literature_provider.get_reader(345) is None


def test_2_1_get_reader_type_error(basic_literature_provider):
    with pytest.raises(TypeError):
        basic_literature_provider.get_reader('not-an-int')


def test_3_0_add_reader_correct(usual_reader):
    lp = LiteratureProvider()
    lp._add_reader(usual_reader)
    res_reader = lp._readers.get(usual_reader.card_num, None)
    assert (
            res_reader is not None
            and res_reader == usual_reader
    )


def test_3_1_add_reader_type_error():
    lp = LiteratureProvider()
    with pytest.raises(TypeError):
        lp._add_reader("not-a-reader-object")


def test_4_0_is_same_reader_exists_corect(basic_literature_provider, usual_reader):
    assert basic_literature_provider._is_same_reader_exists(usual_reader)
    assert not basic_literature_provider._is_same_reader_exists(
        Reader(Name("1", "2", "3"), 456, "other_passport_info"))


def test_4_1_is_same_reader_exists_type_error():
    lp = LiteratureProvider()
    with pytest.raises(TypeError):
        lp._is_same_reader_exists("not-a-reader-object")


def test_5_0_register_reader_correct(usual_reader):
    lp = LiteratureProvider()
    assert lp.register_reader(usual_reader)
    assert not lp.register_reader(usual_reader)


def test_5_1_register_reader_type_error():
    lp = LiteratureProvider()
    with pytest.raises(TypeError):
        lp.register_reader("not-a-reader-object")


def test_6_0_create_order(usual_reader):
    lp = LiteratureProvider()

    reader = usual_reader
    lp._readers[reader.card_num] = reader

    copy0 = ElectronicCopy(0, "electronic author", "anoher title")
    copy1 = PaperCopy(1, "paper_author", "some title")
    lp._copies[copy0.index] = copy0
    lp._copies[copy1.index] = copy1

    added_copies = [copy0, copy1]

    lp.create_order(reader.card_num, [0,1])
    assert len(lp._readers[reader.card_num]._orders) == 1
    for i, copy_in_order in enumerate(lp._readers[reader.card_num]._orders[0].copies):
        assert copy_in_order.type_ == added_copies[i].type()
        assert copy_in_order.index == added_copies[i].index
        if copy_in_order.type_ == TYPE_PAPER:
            pass
        elif copy_in_order.type_ == TYPE_ELECTRONIC:
            assert copy_in_order.link == added_copies[i].get_link()
        assert not copy_in_order.returned
        # date test
