import pytest

from src.classes import Copy, TYPE_ELECTRONIC, TYPE_PAPER


def test_0_0_init_correct():
    index = 1
    author = "auth"
    title = "title"
    c1 = Copy(index, author, title)
    assert (
            isinstance(c1, Copy) and
            c1.index == index and c1.title == title
            and c1.author == author
    )


def test_0_1_init_type_error():
    index = 1
    author = "auth"
    title = "title"
    with pytest.raises(TypeError):
        Copy("not-int", author, title)
    with pytest.raises(TypeError):
        Copy(index, 123, title)
    with pytest.raises(TypeError):
        Copy(index, author, 321)


def test_1_0_eq_correct():
    index = 1
    author = "auth"
    title = "title"
    c1 = Copy(index, author, title)
    c2 = Copy(index, author, title)
    c3 = Copy(2, author, title)
    assert c1 == c2
    assert not c1 == c3


def test_1_1_eq_type_error():
    index = 1
    author = "auth"
    title = "title"
    c1 = Copy(index, author, title)
    with pytest.raises(TypeError):
        c1 == 13232
