import pytest

from src.classes import ElectronicCopy


def test_0_0_init_correct():
    index = 1
    author = "auth"
    title = "title"
    ec1 = ElectronicCopy(index, author, title)
    assert (
            isinstance(ec1, ElectronicCopy) and
            ec1.index == index and
            ec1.author == author and
            ec1.title == title and
            ec1._opened_sessions == 0
    )


def test_0_1_init_type_error():
    index = 1
    author = "auth"
    title = "title"
    with pytest.raises(TypeError):
        ElectronicCopy("not-int", author, title)
    with pytest.raises(TypeError):
        ElectronicCopy(index, 123, title)
    with pytest.raises(TypeError):
        ElectronicCopy(index, author, 321)


@pytest.fixture
def basic_electronic_copy():
    index = 1
    author = "auth"
    title = "title"
    return ElectronicCopy(index, author, title)


def test_1_0_get_link(basic_electronic_copy):
    assert basic_electronic_copy.get_link() == "http://fake_link/{}".format(basic_electronic_copy.index)


# def test_2_0_open_session(basic_electronic_copy):
#     res = basic_electronic_copy.open_session()
#     assert res \
#            and basic_electronic_copy._opened_sessions == 1
#
#
# def test_3_0_close_session(basic_electronic_copy):
#     res = basic_electronic_copy.open_session()
#     assert res and basic_electronic_copy._opened_sessions == 1
#     res = basic_electronic_copy.close_session()
#     assert res and basic_electronic_copy._opened_sessions == 0
#
#
# def test_3_1_close_session_close_without_open(basic_electronic_copy):
#     res = basic_electronic_copy.close_session()
#     assert not res and basic_electronic_copy._opened_sessions == 0
