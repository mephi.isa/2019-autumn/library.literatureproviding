import pytest

from src.classes import Name
from src.utils import *


def test_0_0_init_correct():
    n = "name"
    sn = "surname"
    mid = "middle_name"
    name = Name(n, sn, mid)
    assert (
            isinstance(name, Name) and
            name.name == n and name.surname == sn and name.middle == mid
    )


def test_0_1_init_type_errors():
    n = 1
    sn = "surname"
    mid = "middle_name"
    with pytest.raises(TypeError):
        Name(n, sn, mid)

    n = "name"
    sn = 1
    with pytest.raises(TypeError):
        Name(n, sn, mid)

    sn = "surname"
    mid = 3
    with pytest.raises(TypeError):
        Name(n, sn, mid)


def test_1_0_eq_correct():
    n1 = "name"
    n2 = "another_name"
    sn = "surname"
    mid = "middle_name"
    name1 = Name(n1, sn, mid)
    name2 = Name(n1, sn, mid)
    name3 = Name(n2, sn, mid)
    assert name1 == name2
    assert not name1 == name3


def test_1_0_eq_type_error():
    n = "name"
    sn = "surname"
    mid = "middle_name"
    name = Name(n, sn, mid)
    not_nmae = 124
    with pytest.raises(TypeError):
        name == not_nmae
