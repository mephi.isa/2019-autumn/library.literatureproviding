import pytest

from src.classes import CopyInOrder, TYPE_PAPER, TYPE_ELECTRONIC
from src.utils import *


def test_0_0_init_correct():
    type_ = TYPE_PAPER
    index = 1
    link = None
    due_date = get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=10)
    cio = CopyInOrder(type_, index, link, due_date)
    assert (
            isinstance(cio, CopyInOrder) and
            cio.type_ == type_ and cio.due_date == due_date
            and cio.index == index and cio.link == link
    )
    type_ = TYPE_ELECTRONIC
    index = 1
    link = "default_link"
    cio = CopyInOrder(type_, index, link, due_date)
    assert (
            isinstance(cio, CopyInOrder) and
            cio.type_ == type_ and cio.due_date == due_date
            and cio.index == index and cio.link == link
    )


def test_0_1_init_assertions():
    type_ = TYPE_PAPER
    index = "string!!"
    link = None
    due_date = get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=10)
    with pytest.raises(TypeError):
        CopyInOrder(type_, index, link, due_date)

    type_ = "string!!"
    index = 1
    link = None
    due_date = get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=10)
    with pytest.raises(TypeError):
        CopyInOrder(type_, index, link, due_date)

    type_ = TYPE_PAPER
    index = 1
    link = None
    due_date = "invalid date"
    with pytest.raises(TypeError):
        CopyInOrder(type_, index, link, due_date)

    type_ = TYPE_PAPER
    index = 1
    link = None
    due_date = 44444
    with pytest.raises(TypeError):
        CopyInOrder(type_, index, link, due_date)

    type_ = TYPE_ELECTRONIC
    index = 1
    link = None
    due_date = get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=10)
    with pytest.raises(TypeError):
        CopyInOrder(type_, index, link, due_date)

    type_ = TYPE_PAPER
    index = 1
    link = "some_link"
    due_date = get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=10)
    with pytest.raises(TypeError):
        CopyInOrder(type_, index, link, due_date)


def test_1_0_eq_correct():
    type_ = TYPE_PAPER
    index1 = 1
    index2 = 2
    link = None
    due_date = get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=10)
    cio1 = CopyInOrder(type_, index1, link, due_date)
    cio2 = CopyInOrder(type_, index1, link, due_date)
    cio3 = CopyInOrder(type_, index2, link, due_date)
    assert cio1 == cio2
    assert not cio1 == cio3


def test_1_1_eq_type_error():
    type_ = TYPE_PAPER
    index = 1
    link = None
    due_date = get_due_utc_datetime(get_cur_utc_datetime(), days_to_add=10)
    cio1 = CopyInOrder(type_, index, link, due_date)
    cio2 = 123
    with pytest.raises(TypeError):
        cio1 == cio2
