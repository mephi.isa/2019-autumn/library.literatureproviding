import datetime

DEFAULT_PERIOD = 14
# all datetimes are strings in ISO8601 format (YYYY-MM-DDThh:mm:ss) in UTC

def is_datetime_valid(datetime_in: str) -> bool:
    try:
        assert isinstance(datetime_in, str)
        datetime.datetime.strptime(datetime_in, "%Y-%m-%dT%H:%M:%S")
    except (ValueError, AssertionError):
        return False
    return True


def is_utc_datetime_in_future(datetime_in: str) -> bool:
    assert isinstance(datetime_in, str)
    datetime_ = datetime.datetime.strptime(datetime_in, "%Y-%m-%dT%H:%M:%S")
    cur_datetime = datetime.datetime.utcnow()
    return datetime_ > cur_datetime


def get_cur_utc_datetime():
    return datetime.datetime.utcnow().replace(microsecond=0).isoformat()


def get_due_utc_datetime(start_datetime: str, days_to_add: int = DEFAULT_PERIOD):
    assert isinstance(start_datetime, str) and isinstance(days_to_add, int)
    start_datetime_ = datetime.datetime.strptime(start_datetime, "%Y-%m-%dT%H:%M:%S")
    res = start_datetime_ + datetime.timedelta(days=days_to_add)
    return res.replace(microsecond=0).isoformat()
